import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    console.log("test");
    return [
      <div className="App" key={1}>
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to Purch Ad example on React</h1>
        </header>
        <p className="App-intro">
          Below should load an ad!
        </p>
      </div>,
      <div id="header_ad" key={2}>
      {window.tmntag.cmd.push(function() {window.tmntag.adTag('header_ad', false)})}    
      </div>,
      <div id="in_content" key={3}>
      {window.tmntag.cmd.push(function() {window.tmntag.adTag('in_content', false)})}    
      </div>,      
    ];
  }
}

export default App;
